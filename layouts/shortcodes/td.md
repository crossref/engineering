{{/*  Link to a Tech Debt page. */}}

{{ $path := (printf "/technical-debt/%s.md" (.Get 0)) }}
{{ $page := ref . $path }}
{{ with .Site.GetPage $path }}<a href="{{ $page }}">{{ .Title }}</a>{{ end }}