{{/*  Link to a Decision Record page. */}}

{{ $path := (printf "/decision-records/%s.md" (.Get 0)) }}
{{ $page := ref . $path }}
{{ with .Site.GetPage $path }}<a href="{{ $page }}">{{ .Title }}</a>{{ end }}