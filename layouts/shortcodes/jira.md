{{ $issue := (.Get 0) }}
<a href="https://crossref.atlassian.net/browse/{{$issue}}">{{ $issue }}</a>