{{/*  Link to page in the docs directory. */}}
{{ $filename := (.Get 0) }}
{{ $path := (printf "docs/%s.md" $filename) }}
{{ $page := ref . $path }}{{ with .Site.GetPage $path }}<a href="{{ $page }}">{{ .Title | default $filename }}</a>{{ end }}