## Crossref Engineering Guide

Browse at <https://crossref.gitlab.io/engineering>.

## Living Documentation

This is fetched and built at deploy time in CI. This repo has submodules which are checked out in the `living-docs-modules` directory. To add projects, run e.g.

```
git submodule add https://gitlab.com/crossref/mycrossref.git living-docs-modules/mycrossref
```

The `fetch-living-docs.sh` will make sure these are up to date, translate the Gherkin syntax into Markdown, then copy into the Hugo content directory at `content/living-docs`. Do not check these files into git. They are explicitly ignored in the `.gitignore` file.

## Updating Docsy version
1. Update version number in the `go.mod` file.
2. Run `hugo mod get && hugo mod tidy`

## Test locally

Run `hugo server`
