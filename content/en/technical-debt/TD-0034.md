---
title: "TD-0034: Funder RDF XML data file is parsed using the XML data model not RDF"
authors:
- Joe Wass
status: active
date: 2021-01-11
---


## Where?

Funder ingest

## What's the situation?

- We ingest a SKOS-vocabulary file serialized in RDF-XML.
- We parse it as XML, not RDF-XML.
- Because we incorrectly expect the file to have in a very specific structure, projections from the SKOS to the Fundref model rely on string manipulation and XML-element rewriting.
- If the RDF data model is serialized in a valid but different format, even if the model remains identical, our code cannot parse it.

## What does it make more difficult?

- Elsevier cannot change their serializion format, or even the ordering of the fields, without breaking ingestion.
- Frequent breakages mean that each ingestion turns into toil, and can't be performed by a product owner, only by a developer.
- We can't automatically ingest the file becuase it often breaks.

## How can we improve it?

- Parse the SKOS file using an RDF library and work at the RDF data model level.