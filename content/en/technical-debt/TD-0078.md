---
title: "TD-0078: Deposit update handler in the Cayenne REST API uses email as a notification mechanism"
authors:
- Joe Wass
status: fixed
date: 2021-01-07
---


## Where?


## What's the situation?

We removed the `/v1/deposit` endpoint, so removed all the update handling too.

## What does it make more difficult?

 - More infrastructure to maintain.
 - Tricker to run multiple deployments and test locally.

## How can we improve it?

 - Switch updatable events to an activity stream