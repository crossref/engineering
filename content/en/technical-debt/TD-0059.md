---
title: "TD-0059: Error messages shown to users are produced in a number of different places, backend and frontend"
authors:
- Joe Wass
status: active
date: 2021-01-11
---


## Where?


## What's the situation?


## What does it make more difficult?


## How can we improve it?