---
title: "TD-0074: CS has custom code for serializing JSON and CSV"
authors:
- Joe Wass
status: active
date: 2021-01-07
---


## Where?


## What's the situation?

 - JSON serialization uses a custom library.
 - CSV is often written by string concatenation.

## What does it make more difficult?

 - Delimiter / encoding bugs could easily crop up.
 - It's difficult to extend the code and be confident about it.
 - If there are security holes in the custom library it's difficult to reliably patch.
 - It's more difficult to switch wholesale to a new data format across our services.

## How can we improve it?