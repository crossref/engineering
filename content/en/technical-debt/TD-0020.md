---
title: "TD-0020: Subset of Oracle database is compatible with MySQL, but not all."
authors:
- Joe Wass
status: WIP
date: 2021-01-07
---


## Where?


## What's the situation?

- Some of the Oracle functionality can be run against MySQL. It is on `test.crossref.org`, which has no Oracle, but doesn't provide all of the functionality.

## What does it make more difficult?

Migration to a single RDBMS (see {{< td "TD-0019" >}})

## How can we improve it?

Review code for compatibility with a new database. Test for compatibility.