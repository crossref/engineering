---
title: "TD-0058: We don't have a localisation framework for our user interfaces"
authors:
- Joe Wass
status: fixed
date: 2021-01-11
---


## Where?


## What's the situation?

Fixed. See {{< dr "DR-0450" >}}.

## What does it make more difficult?


## How can we improve it?