---
title: "TD-0093: Event Data Percolator doesn't have access to DOI and 'ownership' metadata so it has to create a fuzzy mapping"
authors:
- Joe Wass
status: active
date: 2021-02-24
tags:
 - event-data
---


## Where?

Event Data Percolator

## What's the situation?

Percolator (and Event Data Agents) have no access to the internal data structures, such as owner prefixes. This means that the landing page sampler has to infer complicated many-to-many relationships based on sampling and linking DOI literal prefixes to observed domains.

Because DOI transfers, the literal DOI prefix has no direct connection to the member, which is the true place that we should maintain lists of landing page domain names. The Event Data agents therefore have to work with a fuzzy mapping of domains to prefixes. 

This is closely related to crossref/technical_debt#94

## What does it make more difficult?

Curation of the domains-decision artifact is very difficult. The results we return in Events contain extra complexity that may be hard to interpret. The landing [page flow chart](https://www.eventdata.crossref.org/guide/data/matching-landing-pages/) documents this complexity.

![landing-page-flow](/uploads/0a3978cdc8b0800aa91079604e3bb90d/landing-page-flow.png)

## How can we improve it?

Refector community data model to provide that information, so that domains can be matched to DOIs more precisely. Then the community data model can then verify whether a given DOI corresponds to a landing page domain or not.