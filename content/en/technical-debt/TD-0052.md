---
title: "TD-0052: For identifiable entities (titles, funders, authors) we accept both identifiers and free-form metadata."
authors:
- Joe Wass
status: active
date: 2021-01-07
---


## Where?


## What's the situation?


## What does it make more difficult?


## How can we improve it?