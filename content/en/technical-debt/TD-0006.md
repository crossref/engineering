---
title: "TD-0006: There is a large amount of copy-pasted code between AbstractMetadataProcessor subclasses, each with some subtle changes."
authors:
- Joe Wass
status: active
date: 2021-01-11
---

## Where?

AbstractMetadataProcessor derivatives in the submission system.

## What's the situation?

The `processMe` method in the subclasses of [AbstractMetadataProcessor](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/AbstractMetadataElement.java) have substantial blocks of copy-pasted code. The `processMe` methods have the following lengths, each with a lot of code in common:

 - [DissertationElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/DissertationElement.java#L299) - about 500 lines
 - [StandardElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/StandardElement.java#L332) - about 480 lines
 - [GrantElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/GrantElement.java#L95) - about 170 lines
 - [PeerReviewElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/PeerReviewElement.java#L177) - about 260 lines
 - [JournalElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/JournalElement.java#L289) - about 950 lines
 - [ConferenceElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/ConferenceElement.java#L248) - about 800 lines
 - [PendingPubElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/PendingPubElement.java#L244) - about 300 lines
 - [PostedContentElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/PostedContentElement.java#L204) - about 480 lines
 - [BookElement](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/ds/metadata/BookElement.java#L300) - `processBookMetadataElement` has 570 lines, `processContentItems` has about 670 lines

## What does it make more difficult?

 - We can't test a part of the deposit process.
 - Therefore we can't update it confidently.
 - Fixing a common bug across each method requires fixing and testing a lot of different source files.
 - We can't easily see if the copy-pasted code behaves exactly the same, or if there are subtle differences.

## How can we improve it?

 - Investigate each step in each processor and correlate them.
 - Refactor out common code.