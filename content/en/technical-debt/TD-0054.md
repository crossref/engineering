---
title: "TD-0054: Usernames and passwords are supplied using a range of parameters on different endpoints."
authors:
- Joe Wass
status: active
date: 2021-01-07
---


## Where?


## What's the situation?


## What does it make more difficult?


## How can we improve it?