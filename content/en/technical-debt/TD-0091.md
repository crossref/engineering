---
title: "TD-0091: Event Data Evidence Logs are in CSV and JSON formats, but no-one has used both. JSON will do."
authors:
- Joe Wass
status: active
date: 2021-01-18
tags:
 - event-data
---


## Where?


## What's the situation?

- Forcing them to be in both formats limits the flexibility.

## What does it make more difficult?


## How can we improve it?