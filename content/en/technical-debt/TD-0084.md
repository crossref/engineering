---
title: "TD-0084: MemberInfo file is a Java serialized object"
authors:
- Joe Wass
status: active
date: 2021-06-17
tags:
 - content-system
 - member-info
---


## Where?

Content System member info

## What's the situation?

The MemberInfo file is built from a Sugar ingestion and from the owner prefix structures in the database. The file is serialized using the [Java serialization](https://docs.oracle.com/javase/8/docs/platform/serialization/spec/serialTOC.html) framework. This file is then distributed.

## What does it make more difficult?

1. Distribution of the file only happens wholesale. 
2. Servers that have the dependency (ds and qs, and others besides) can't start without a memberinfo file on disk. This makes scaling out infrastructure tricky. 
3. Introduces coupling to the specific bytecode version. Changes to the memberinfo structure / bytecode signature mean manual generation and copying of the file.

## How can we improve it?

Move it to be in database tables.  

## See
 
 - {{< td "TD-0016" >}}
 - {{< td "TD-0083" >}}
 - https://gitlab.com/crossref/issues/-/issues/1000
