---
title: "DR-0069: AWS Elastic search Service vs self-run"
status: "accepted"
deciders: 
- Joel Schuweiler
- Geoffrey Bilder
authors: 
- Joe Wass
date: "2020"
tags:
- database
- infrastructure
- elastic
---

Technical Story: «description | ticket/issue URL»

## Context and Problem Statement

As part of [[DR-0010]] we will be running Elastic Search in the cloud. We already run Elastic Search for Event Data in AWS, although it's configured on top of EC2 with Terraform. 

## Decision Drivers 

- Cost
- Reliability
- Scalability

## Considered Options

* Run it ourselves, provision EC2 machines, configure, etc.
* Use the hosted AWS service

## Decision Outcome

1. Use the hosted AWS Elastic Search service for migrated REST API.
2. Switch to the AWS Elastic Search service for Event Data.

### Positive Consequences 

1. Less maintenance.
2. Trust AWS to get configuration right.
3. Tighter integration.


### Negative Consequences 

1. Approx 50% more expensive per capacity.
2. Smaller feature set which constrains our ability to provide Plus / Public / Polite pools.

## Links 

* <https://aws.amazon.com/opensearch-service/>

