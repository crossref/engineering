---
title: Decision Records
weight: 20
menu:
  main:
    weight: 30
description: >
   Important, expensive, critical, large scale or risky architecture decisions including rationales

cascade:
- type: "blog"
---

We want to design infrastructure that lasts for a long time. This log describes significant decisions made since 2019, in approximate chronological order. This is where we explain how we build our system over time, how we made decisions, and the context at the time.