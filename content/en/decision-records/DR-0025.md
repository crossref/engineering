---
title: "DR-0025: All code in the same source control system"
status: "accepted"
deciders: 
- "Geoffrey Bilder"
- "Chuck Koscher"
- Joe Wass
- "Joe Aparo"
date: "2019-06-26"
tags:
- development
- infrastructure
- content-system
---

## Context and Problem Statement
Content System has historically been stored in Subversion in a data centre installation. Labs projects (including CrossMark, Content System, Cayenne REST API, Event Data) have been stored in Git in GitHub.

This adds a hurdle to collaboration and prevents us from sharing process improvements across codebases. 

See also [[DR-0020#Issue tracking across software projects and product areas]]

## Decision Drivers 
- Ease of onboarding new developers and letting developers move between projects.
- Standardise on automation, e.g. CI/CD tooling, issue tracking

## Considered Options
* GitHub
* GitLab

## Decision Outcome
- All code in GitLab.
- Repos moved from GitHub to GitLab.
- SVN codebase converted to Git.
- Content System codebase still private repo, as it contains sensitive information.

### Positive Consequences 
* Easy for community to find our source code repositories.

### Negative Consequences 
* Some effort involved in migrating.

## Pros and Cons of the Options 

### GitHub
- At the time of deciding, doesn't make it easy to collaborate on issue tracking over more than 10 projects (see [[DR-0020#Issue tracking across software projects and product areas]]).
- CI/CD inferior to GitLab.

### GitLab
- Allows issue tracking across multiple projects.
- CI/CD built in.

## Links 
* [Technology Update 1](https://docs.google.com/document/d/13pdR1Np5s3QFhiiZCTkpaQIuFwWrg2uXuC8jy051jI4/edit)
* [Technology Update 2](https://docs.google.com/document/d/1vQj7Uih1EdzZylPZ-HLV3U_xV5xxdrQmxRb3de9_V7c/edit)

