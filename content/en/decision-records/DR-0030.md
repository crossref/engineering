---
title: "DR-0030: Users as Legacy Roles"
status: "accepted"
deciders: 
- Joe Aparo
- Geoffrey Bilder
- Joe Wass
authors: 
- Carlos del Ojo Elias
date: "2019-07-01"
tags:
- design
- auth
- content-system
---

## Context and Problem Statement
CS used to be the place where we were centralising our user database. Those users were stored in an Oracle database table called REMOTEUSERS, and the username format was simply a plain alphanumeric word. No information about the member like the email address was stored in the table. These accounts were usually shared among different users within the same company.

In order to reset passwords, the users needed to contact support, and the process was a manual process involving human intervention, without being possible to automate without the member email address. Because user accounts could be shared by several people, if one person happen to reset the password, that account wouldn't be operational anymore for the rest of the users as the password would be different.

On the other hand, because CS is tightly coupled to the authentication process, it is difficult for other systems to authenticate without going through CS.

## Decision Drivers 
* Remove the toil of resetting passwords.
* Allow multiple users of the same organisation have their own credentials without generating conflict with other users
* Isolating our authentication services, so they can be used by other Crossref services.
* Enabling the use of token (HTTP Authorisation header)

## Considered Options
1. Keep Users, rename to "legacy roles". Add email-based users who can assume Legacy Roles.
2. Migrate all users (> one per member).

## Decision Outcome
Current users will be kept in the same database and CS will work in the same way, those users will be known as "legacy roles". A new external system known as `authenticator` will allow registering new users using email addresses and allowing password reset flows. Authenticator will allow the creation of roles as well, those roles will have to be present in the CS REMOTEUSERS table. Users created in Authenticator will be mapped (many to many) to roles so that a user will be able to choose what role do they want to use when logging in. If they are mapped to only a single role they will not be asked what role to choose so that they will be immediately authenticated. 

In this way, all users are migrated to authenticator using their old user identifier (instead of email address). They will be upgraded to email address as soon as they try to reset their password. Linking that new user to their original legacy role.

New concepts: 

* Legacy roles: users stored in the REMOTEUSERS oracle table used by CS
* Legacy credentials: All credentials will be moved to `authenticator` once it starts running. They will not be email address credentials, but usernames identical to the legacy role (alphanumeric word), which will be mapped one to one to the legacy role with the same name. If users contact support to reset the password they will get new credentials (email address credentials) and these credentials will be mapped to the legacy role.
* Email credentials: credentials given to either new users or users who had legacy credentials and wanted to renew their passwords, that credential is linked to the corresponding legacy role.

When users perform any operation (eg. depositing) authenticating with their credential, it doesn't matter whether it is legacy or email as it is mapped to a legacy role who is the ultimate actor for that operation. The system is still not registering the original identity of the actions, so the only actor is the organisation itself.

### Positive Consequences 
* It will allow users to reset their own password once they've been moved to email credentials
* Allows for multiple credentials per organisation without conflict between users
* Externalised the authentication allows for other services to authenticate users

### Negative Consequences
* Adds complexity in the user/role model: Legacy roles <-> Email/legacy credentials
* We still do not trace identities but organisations identified by legacy role.

## Links
These links are all for background and non-normative. The number of documents here is a good argument for a decision register!

* [Technology update 5](https://docs.google.com/document/d/1vUB5Izkn4lqLJXDyttUa_qCkgjvDt2LjB6MO6S1kva8/edit)
- Background: [Authentication workflow multiple credentials](https://docs.google.com/document/d/1QauRXykc-CphmcjdXjZN4dr-ea6XC6XHGEzDd1xmX44/edit)
- Background: [Auth Services - Technical Overview](https://docs.google.com/document/d/1JjZi2j5ppDdabKjSpBR2r1EESX1n0_ndFvofir23JtM/edit)
- Background: [Authentication - concept of roles](https://docs.google.com/document/d/12eCQzyn5an9zxqZ8T2LQRT8oxsQQ73w7NCKmw0c6m2g/edit#heading=h.ag9cg710tm61)
- [Auth project update](https://docs.google.com/document/d/1fcygWASFYFaZLvJTi0ZO3T7EWj4mIPkJNeLniwa86Zk/edit#heading=h.xaz126bol2gi)
- [Auth Project Implementation](https://docs.google.com/document/d/1F2EZ2aDz-NHS3nlSkk9LCPuLAWlFkjsHb3ipUBt9T9Y/edit#heading=h.2vzxcrkvnewy)
- [Auth and Auth Design](https://docs.google.com/document/d/1JsgUFaDJrrAo3861OPbVTZzYLiJXWGbymWenDXT-Oqk/edit#heading=h.w8nd1k9q6b0s)
- [CS / Authenticator Sync](https://docs.google.com/document/d/1rVJEOmz3AJ4zA1m_UYGTbPdqH4H-sAvEKNXgo0FYPsU/edit)
- [Authentication rollout plan](https://docs.google.com/document/d/1gKeSL8iEDQscn5WGR59pOoC1agaTPTWmVJy7Y_nXnrk/edit#heading=h.utqrxzfr7a92)
- [Auth project status](https://docs.google.com/document/d/1TjSRczTuvHUrY6Ftdo_WlAcy1iXwsASh58aiuFEU7Gk/edit)
- [CS Authorisation surgery](https://docs.google.com/document/d/1Vs8p635fhnZFsr7NJ8YxsB9BEXKksvi_79NrM5Mnh8M/edit#heading=h.nczdotnd15yv)
