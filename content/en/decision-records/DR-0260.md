---
title: "DR-0260: XState State machines for front-end state and Vue Router for routing"
tags:
- framework
- front-end
status: "accepted"
deciders: 
- "Patrick Vale"
date: "2022-03-24"
---

## Context and Problem Statement

As we develop user-facing applications, need to store the application's state and extended context.

This includes user preferences, user-created content, and user identity. These states and extended contexts should be easy to create, update, traverse, and reason over.

The model of the flow of state and context through the application should be accessible to developers, designers and product managers.

## Decision Drivers 

* Helping developers reason over the application state
* Helping designers specify the flow through the application
* Surfacing complexity, declaring state transitions explicitly, avoiding corner-cases

## Considered Options

* Vue `$root` level data object
* Vuex (redux-style global state store)
* XState and xstate-vue2

## Decision Outcome

Chosen option: "XState and xstate-vue2", because it allows the modelling of complex application flows and contextual data within the same library, and can be visualised and used interactively alongside the running application.

### Positive Consequences 

* User Interface complexity is surfaced early, not hidden
* Paths through applications states are clear
* Context management is declared alongside state management

### Negative Consequences 

* Developers need to learn a new library and design pattern
* There is a learning curve

## Pros and Cons of the Options 

### Vue `$root` level data object

[https://gitlab.com/crossref/frontend/-/blob/master/src/index.js#L133] 
[https://gitlab.com/crossref/frontend/-/blob/master/src/components/regular/Endpoint.vue#L4]

* Good, because it's very simple to implement
* Bad, because it doesn't scale well
* Bad, because it's [not recommended](https://v2.vuejs.org/v2/guide/components-edge-cases.html#Accessing-the-Root-Instance) by the upstream framework for anything but the smallest of apps 

### Vuex

https://vuex.vuejs.org/ 

* Good, because it's the [default recommendation](https://v2.vuejs.org/v2/guide/components-edge-cases.html#Accessing-the-Root-Instance) from Vue.js
* Good, because it has good community support
* Bad, because it doesn't model state transitions explicitly

### Xstate + xstate-vue2

https://xstate.js.org/

* Good, because it models states and the possible transitions between them explicitly
* Good, because it handles context alongside state transitions
* Good, because it provides and interactive visualiser running alongside the application
* Bad, because it has a learning curve

## Links 
