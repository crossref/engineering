---
title: "DR-0330: Replace Event Data's ‘occurred date’, with various Item properties assertions"
status: 
deciders: 
- Martyn Rittman
- Panos Pandis
- Joe Wass
date: "2022-05-06"
tags:
- TODO
---

Technical Story: «description | ticket/issue URL»

## Context and Problem Statement

Dates are complicated! We can remove the concept of an ‘occurred date’, but record the various date types (edit, update, observed, etc.) as properties of items and statements.

«Describe the context and problem statement, e.g., in free form using two to three sentences. You may want to articulate the problem in form of a question.»

## Decision Drivers 

* «driver 1, e.g., a force, facing concern, …»
* «driver 2, e.g., a force, facing concern, …»

## Considered Options

* «option 1»
* «option 2»
* «option 3»

## Decision Outcome

«Chosen option: "«option 1»", because «justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | … | comes out best (see below)».

### Positive Consequences 

* «e.g., improvement of quality attribute satisfaction, follow-up decisions required, …»
* …

### Negative Consequences 

* «e.g., compromising quality attribute, follow-up decisions required, …»
* …

## Pros and Cons of the Options 

### «option 1»

«example | description | pointer to more information | …»

* Good, because «argument a»
* Good, because «argument b»
* Bad, because «argument c»

### «option 2»

«example | description | pointer to more information | …»

* Good, because «argument a»
* Good, because «argument b»
* Bad, because «argument c»


### «option 3»

«example | description | pointer to more information | …»

* Good, because «argument a»
* Good, because «argument b»
* Bad, because «argument c»


## Links 

* [Evidence Records to Manifold Mappings](https://docs.google.com/document/d/1BkNPAlwhAxyiAqJwIQbDpmslT2kuAgAszIpa3YALwig/edit#)

