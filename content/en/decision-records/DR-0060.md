---
title: "DR-0060: SONAR Test coverage quality gate"
status: "accepted"
deciders: 
- "Joe Aparo"
- Joe Wass
date: "2020-05-19"
tags:
  - development
---

## Context and Problem Statement

New code should be high quality and remain so. 

Old code has variable quality, and we want to improve it. We want to feel that we're making progress.


## Decision Outcome
- SONAR is used in new projects.
- SONAR quality gate is included in CI files for automatic checking as part of build.
- Builds that fail the SONAR quality gate are treated as broken.
- SONAR Cloud subscription.

### Positive Consequences 
* We catch more bugs.
* Code review has an objective baseline.

### Negative Consequences 
* Builds might break if failing code is checked in.
* It's hard to back-port to legacy software.
* Doesn't apply to every language, so we may need a different approach for Clojure and JavaScript / TypeScript.

## Links 
* [SONAR Cloud](https://sonarcloud.io/projects)