---
title: "DR-0280: Every Item in the Item graph should be of a recognised type"
status: "accepted"
deciders: 
- Joe Wass
- Dominika Tkaczyk
- Patricia Feeney
- Martyn Rittman
date: "2022-04-26"
tags:
- design
- metadata
- item-graph
---

Technical Story: [CR-334](https://crossref.atlassian.net/browse/CR-334) epic: [CR-73](https://crossref.atlassian.net/browse/CR-73)

## Context and Problem Statement

### Different vocabularies

There are a number of types of entities across our systems that represent various bits of the research nexus. Bringing these together in the item graph means that we need to find a consistent vocabulary for types of things. This reconciliation extends not only to top-level types (e.g. members vs works) but the second layer of types (e.g. types of works).

In addition to this, we model our metadata schema in at east 3 ways: the XML schema itself, the implementation within Content System, and the Item Tree representation. Per [[DR-0205]] we have chosen to use the Item Tree in the Item Graph because it is already in the right format, and it has proven to give useful results as part of the REST API.

Here is a non-exhaustive illustration of existing 'types' taxonomies across our metadata:

- REST API top-level types: work, member, prefix etc
- REST API work subtypes: journal, journal-article etc
- Schema content types
- Content System Genre types
- Content System Item Types
- Funder registry schema
- REST API funder information

Each vocabulary applies to an entity that we will model as an Item.

### New entities

The Item Graph involves some new kinds of entities that result from the reification of relationships, such as 'citations', 'landing pages' etc. See [[DR-0270]] for example. These are second-order effects resulting from our entity modelling. They are not not just side-effects of the data model though, they provide concrete value. By splitting out citations, landing pages, etc, we enable reference matching with accurate provenance attribution (e.g. the member asserts the unstructured citation, Crossref asserts the match to the DOI). And by opening up this data model we enable others to understand and participate in it. See discussion in [[DR-0125]].

We will likely need to create types to describe these new Items. 

## Decision Drivers 

1. Allow merging of data from many systems (XML Schema, Content System administrative info, Item Tree, Event Data, Funder Registry and more)
2. Create a vocabulary that's useful for users of the metadata and consistent, and can be translated into, and out of, external vocabularies.
3. Create a vocabulary that's useful for transactional item tree, e.g. reified items useful for reference matching.

## Considered Options

1. Don't try and give Items types.
2. Do assign types to Items, and maintain a vocabulary of them.

## Decision Outcome

Option 2, we do assign types to Items and maintain a vocabulary of types.

### Positive Consequences 

1. We have a shared model for migrating our various systems.
2. We can publish a vocabulary that describes our model for users of our APIs.
3. By exposing this as a data-driven ontology, we can extend it without writing new code and ensure that it's automatically self-documenting.

### Negative Consequences 

1. We need to resolve some conflicts.

## Links 

* [Discussion doc](https://docs.google.com/presentation/d/1nEmcIzax_tsvbzcUCMf4H77ThDnohrmT3paoTLASbk8/edit#slide=id.p)
