---
title: "DR-0310: Double authentication systems management"
status: "accepted"
deciders: 
- Sara Bowman
- Carlos del Ojo Elias
- Mike Gill
authors: 
- Carlos del Ojo Elias
- Joe Wass
date: "2022-05-24"
tags:
- design
- auth
- plus
- mycrossref
---

Technical Story: [CR-1](https://crossref.atlassian.net/browse/CR-1)

## Context and Problem Statement
Authenticator is the current authentication mechanism working in production. It gives access to all services (i.e. those hosted in Content System) except for PLUS access. 

Keycloak is an IDM that will be introduced initially for authenticating / authorising PLUS subscribers. It will manage not only access keys, but user accounts that PLUS subscribers use to log in to get their keys. 

Some plus subscribers are also Crossref members as well and some are not. *Users* from Members will have credentials in Authenticator. *Users* from PLUS subscribers will have credentials in Keymaker. It's therefore possible for a given user to have different passwords in each service. This is confusing, as we want users to be able to treat Crossref as as single service and not worry that we have two separate accounts systems.

## Decision Drivers 
* avoid confusing end user experience
* limit support team toil
* maximise self-service for users
* complexity of solution

## Considered Options
* Option 1: Having 2 separate auth systems where passwords can drift out of sync. Enable separate password reset flows for each (default)
* Option 2: Syncing credentials from Authenticator to Keycloak. Allow password resets only via Authenticator.

## Decision Outcome
Option 2: Syncing credentials from Authenticator to Keycloak.

### Positive Consequences 
* Minimal impact on the end user. It will enforce both authentication systems to be in sync, so resetting your password will not be a problem.
* Avoids the confusing situation.
* Avoid support team having to ask which system the user is asking about.

### Negative Consequences 

* Keycloak credential management flows will be hidden for the end user until the full migration happens, which may be a significant length of time.
* Syncing exceptions will have to be handled appropriately, that means increase in complexity of the systems and extra point of failure.

## Pros and Cons of the Options 

### 1: Having 2 separate auth systems out of sync
* Good, low complexity, old and new authentication systems disconnected.
* Bad, when users change password in one system, it will differ from the other system causing confusion.
* Bad, two reset flows to maintain, along with password complexity requirements etc.

### 2: Syncing old and new systems
Old system Authenticator will be the only system that will allow password resetting, communicating it directly to Keycloak, so it gets updated, making sure both systems are in sync.

The solution here involves enabling only one system in order to perform password reset actions. Because Keycloak password reset flow is a Keycloak feature, syncing back to authenticator (custom software) is complicated. However our members are already used to the old authenticator reset flow, that can be tweaked to sync its password hashes with Keycloak via Keymaker. Keycloak password reset flow would be disabled so systems cannot get out of sync. This solution can be kept up until we have completed full migration to Keycloak. Then password reset flow would be enabled in the new system.

* Good, because both systems will be in sync, so the end user won't get confused when changing the password in Authenticator.
* Bad, because increases complexity modifying authenticator and handling potential syncing exceptions.

## Links 

* [Modify Authenticator to sync with keycloak/keymaker](https://crossref.atlassian.net/browse/CR-552)

