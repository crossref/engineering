---
title: Maintenance
---

We rotate maintenance duty for our codebases within the team. This is a part-time duty alongside feature work, although high importance bugs may be prioritised.

Each codebase needs different expertise, so they are rotated separately. These are broken out in the rota (WIP).

### Responsibilities

 1. Be the point of escalation for bugs as they arise.
 2. Monitor Sentry and error logs and file and discuss bugs.
 3. Participate in discussions around prioritisation.
 5. Refine bug reports in the backlog.
 6. Research and fix bugs in the backlog.
 7. Periodically review dependencies for codebases in maintenance, keep them up to date.
 8. Identify improvements in the code and services, and removing impediments to our serenity. For example toil.

When there's active feature work on a codebase it may make more sense to fold some of these into routine development (e.g. review dependencies).
