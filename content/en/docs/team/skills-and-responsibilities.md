---
title: Development Team Responsibilities and Skills
---

## Job Responsibilities


 - Understand Crossref’s mission and how we support it with our services
 - Pursue continuous improvement
 - Work flexibly in multi-functional project teams to design and develop services and ensure that our systems are reliable, responsive, and efficient.
 - Work closely with the Head of Software Development to solve problems, maintain and improve our services and execute technology changes.
 - Provide code reviews and guidance to other developers regarding development practices and help maintain and improve our development environment
 - Identify vulnerabilities and inefficiencies in our system architecture and processes, particularly regarding cloud operations, metrics and testing
 - Communicate proactively with membership and technical support colleagues ensuring they have all the information and tools required to serve our users
 - Openly document and share development plans and workflow changes
 - Be an escalation point for technical support; investigate and respond to occasional but complex user issues; help minimize support demands related to our systems; respond to service outages.
 - Where possible, ensure that what we do can be built on by the open source community.


## Skills

These are skills that we value in the Software Development team at Crossref. 

### Domain Modelling

Collaborating with the Metadata and Product teams.

| | |
|-|-|
| **Data Wrangling** | Working with messy data and making sense of it. |
| **XML Schema** | Understand, interpret and design XML Schema. Modelling bibliographic information to data structures. |
| **JSON Schema** | Understand, interpret and design JSON Schema. Modelling metadata in JSON Schema. Building maintainable models that can be extended and built on. |

### Specification

Collaborating with the Product team.

| | |
|-|-|
| **Prototyping** | Building actionable prototypes for technical proof of concept, or user feedback |
| **User testing** | Working with users directly to understanding their needs |
| **Iteration** | Working with Product Owners to iterate on an executable specification. Creating iterable steps. | 
| **Behaviour Driven Design** | Following the process described in the book Writing Great Specification. Ensuring that all specification points can be tested. |
| **API Design** | Build APIs that serve both internal users and stakeholders in the organization and the community. | 

### Infrastructure

Collaborating with the Infrastructure team.

| | |
|-|-|
| **Packaging for deployment** | Designing efficient build files. Docker images.  |
| **Metrics** | Design and expose metrics that are useful for cloud deployment and scaling. |
| **Cloud-first software design** | Stateless, scalable code. Technology design choices informed by POSI. |

### Operational

| | |
|-|-|
| **Release management** | Coordinate releases. Bring in stakeholders, communicate timelines. Supervise deployments. |
| **Source control** | Use git to branch, merge and release. According to our guidelines. | 

### Project management

| | |
|-|-|
| **Managing a project** | Structuring a project and deliverables | 
| **Negotiating scope** | Work with Product Owner and other stakeholders to scope work. Ensuring that, from the software side, the work is doable and has low risk. | 


### Programming

| | |
|-|-|
| **Languages** | Expert understanding of our chosen programming languages. |
| **Testable Code** | Writing well-factored, testable code. |
| **Secure** | Writing secure code that conforms to standard practices. |
| **Frameworks** | Understanding and making best use of frameworks, where they apply. |
| **System architecture** | Designing end-to-end solutions that fit with, and extend, the current architecture. Owning the scope of the work, ensuring it is documented and testable. | 
| **Refactoring** | Adjusting for better extensibility, modelling, etc. |
| **Code review** | Supporting team members by reviewing code, providing feedback. |

### Front-end

| | |
|-|-|
| **Building user interface designs** | Designing and building user interfaces.  | 
| **Automatic testing for user interface design** | Ensuring that UIs are tested, and that the tests are useful indicators of the specification. | 
| **Accessibility** | Ensuring that user interfaces are accessible, using standard practice. | 
| **Browser compatibility** | Ensuring that user interfaces are compatible with all supported browsers. | 

### Maintenance

| | |
|-|-|
| **Writing maintainable code** | Write code that can be maintained and extended, and is easy to debug. |
| **Debugging and fixing bugs** | Tracking down reported issues. | 
| **Regression testing** | Documenting and regression-proofing bugs with tests. |
| **Migration** | Migrating legacy code and functionality to greenfield. Improving quality and test coverage. | 

 
### Writing

| | |
|-|-|
| **Internal documentation** | Producing technical internal documentation for developers. |
| **User-facing documentation** | Contributing to written public user-facing documentation. |
| **Asynchronous communication** | Writing in a way that's effective foraudiences across timezones. |


## Database

| | |
|-|-|
| **SQL** | Writing efficient SQL queries in the technology of our choice. |
| **Debugging** |Debugging SQL performance issues. |

