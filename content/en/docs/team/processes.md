---
title: Team Processes
aliases:
- /docs/tools-and-processes/processes/
---

### Product led

All of the software development work we do is prioritised by Product owners. Developers will not start any work unless it has been requested by a product owner. Do not write a line of code unless it's answering a clearly defined request from a product owner. All work should be connected to a Jira ticket, contained in an Epic. 

Product owners are responsible for gathering stakeholders and defining scope. We may not know the solution up-front, but they provide a framework for answering questions as we go.

When designing APIs and schemas, developers will play a central role in the design. Every element should answer a well understood product requirement. The Product Owner is responsible for consulting stakeholders when evaluating the design. Note that the development team, and product owners, are stakeholders on all API design, as we build services on top of services.

Work must be reported back to the product owner preferably daily, and never more than 3 work days. 

If there are questions about how things are implemented, do a a time-boxed investigation. 

### Independent teams

Teams form to work on a feature, whether that's a whole epic or a single user story. A team (called the 'three amigos' in the book), has a Product Owner and Lead Developer.

 - The team is jointly responsible for understanding the problem and the eventual solution. There will be various stakeholders in implementation decisions outside this group.
 - The developer is responsible for the implementation, plan, technical decisions and quality of the solution. 


### Documenting work

There are various ways we document what we're working on. Some are ephemeral (they can safely be deleted), some are durable (must not be deleted).

**Ephemeral documentation** is a tool for getting the job done. Jira contains the starting point for work that we do. For example, bug reports, specific user stories, initial acceptance criteria, etc. Jira tickets are not a permanent record of decisions or specifications. They considered disposable after they are completed. Jira ticket should be updated as we work on them, but once we finish, we move on. 

 1. [Jira](https://crossref.atlassian.net/jira/software/c/projects/CR/boards/11): Work we are planning in future, or currently working on. 
 2. [Epic Technical Tactics](https://docs.google.com/document/d/1F3_illZ2pePOpH5Y2g8ykpltnphoUzZXAjNJANr_-iA/edit) is where we discuss technical implementation plans, if necessary.
 

**Durable documentation** is there for the long term, and must be maintained:

 3. [Decision Records]({{< relref "/decision-records" >}}) are for recording significant technical decisions, and how we made them.
 4. BDD Feature Files in source code. These end up in the [Living Documentation]({{< relref "/living-documentation" >}}).
 5. Code documentation (comments, readmes, javadoc, dokka, swagger). 

(Public documentation on our website is out of scope here).

We can link from ephemeral to durable documentation. For example, link from Jira to the BDD feature file.

### Writing a techncial plan

Writing a technical plan may be needed for some pieces of work. For example, if there are a few stages to the plan, it affects other parts of the system, or it needs feedback. All discussions should be asynchronous first. Add to [Epic Technical Tactics](https://docs.google.com/document/d/1F3_illZ2pePOpH5Y2g8ykpltnphoUzZXAjNJANr_-iA/edit) document.

The lead developer is responsible for the implementation plan, identifying stakeholders, and getting feedback.

### Specifications evolve over time

Writing high specifications a professional skill to be practiced, just like writing code. We follow the [Writing Great Specifications](https://www.manning.com/books/writing-great-specifications) book. Get a copy!

The Jira ticket should have a clear initial acceptance criteria. That doesn't mean the final specifications, but enough for the developer to start working on the feature. We may not know the solution up-front, but it's always possible to define acceptance criteria. These may sometimes a question, but it must be possbile to answer it. 

BDD Scenarios are part of the definition of the acceptance criteria. As the feature evolves, the scenarios should evolve. As we write code, we should update the scenarios. The 'markdown field' in Jira is a good place to draft scenarios collaboratively. But remember it's ephemeral. What matters is the specification that's in source control and executed.

If you need to fix a bug, make sure you can reproduce it in a documented regression test (unit, integration, BDD) before doing the fix. 

### Steps

Lead developer is responsible for the ticket for its whole journey. Product owner and tech lead jointly responsible for understanding the problem, and agreeing on the solution. 

1. Product owner + developer move a Jira ticket into 'refinement' to discuss initial scope. It's ready once you have a clear understanding of the problem, or the direction you will follow. 
2. Developer assigns themselves a ticket. The assignee is responsible for that ticket until it is completes. No-one should assign a ticket to someone else.
3. Branch off `main` for your feature or fix. 
5. Discuss the BDD feature files as you work. Push draft merge requests so the product owner can see the spec as it evolves. 
6. When it's time to code review, the lead developer should find a reviewer, assign the merge request to them for review review. Ping them on Slack too. 
7. When it's time to deploy, send a message in the relevant Slack channel (operations, manifold, metadata-retrieval, etc). Keep an eye on it during the deploy.
8. Update the body of the Jira ticket to link to the feature file in the main branch before closing it.

### Branches and commits

 - Branch name must include the Jira ticket `fix/CR-XXXX` or `feature/CR-XXX`. Jira will automatically find the branch and link.
 - Releaseable changes code must be `fix` or `feature` (standard for Semantic Release).
 - Try to split out non-releasable changes into `docs`, `refactor`, `style`, `test` if you can.
 - Commit message must be conventional commit format. If you don't, your feature won't trigger semantic release. Commitizen is recommended to help you get it right. 
 - Include the Jira ticket number in the footer (last line of the commit message).

### Merge Request as a unit of collaboration

Sometimes you need to go away and hack at a problem to understand it. Draft merge requests are a great way to do this and get feedback.

As you evolve your branch you may want to rewrite your feature branch HEAD and force-push:

```bash
git commit --amend
git push origin BRANCHNAME --force
```

Or squash-merge in the merge request. But remember to check the commit message. 

