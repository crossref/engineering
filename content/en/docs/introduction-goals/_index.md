---
title: "Introduction and Goals"
linkTitle: "Introduction and Goals"
weight: 10
description: >
   Requirements, quality goals, stakeholders
tags:
---


## Documentation Goals

This documentation site describes how we build greenfield code and our migration path from our legacy codebase. This includes 

- history and reasons for change (see [Technical Debt]({{< ref "/technical-debt" >}})),
- decisions we make along the way (see [Decision Records]({{< ref "/decision-records">}}) and {{< link "crosscutting/_index" >}}) 
- along with {{< link "source/_index" >}}.

This site does not aim to completely describe our legacy code, such as {{< link "source/content-system" >}}.

## Team Goals

### 1 — See Crossref as one single system

**When build new featuress will look at the Crossref system as a whole.**

This means avoid single-purpose APIs. Instead we build one single API with multiple features (e.g. fields, filters). If we introduce new forms of data we should think about how they apply to our API and output schemas. As we specify functionality we should think about how it applies to the system as a whole.

Every feature we build will be end-to-end testable, where appropriate. We are building a test framework that will be able to look at our whole system (excluding Legacy Content System which we will mock out). 

We look at technical debt of the whole system. When we improve migrate old code, we will clean up the leagacy system and remove unused code.

If we write features that are implemented across more than one codebase, the BDD scenarios should be written in a consistent manner so that the logic can be followed across codebases. If possible, we should testing the whole system. 

### 2 — Keep our system running

We have a many subsystems in production. We should continue to meet the expectations of our members and users. This includes fixing bugs and addressing toil that arises, doing maintenance work, and making improvements that help us meet this goal.

### 3 — Eliminate mystery

Across legacy Content System, Cayenne and the other codebases running, and attached systems such as SugarCRM, there's a lot of behaviour that is still not understood by all stakeholders. We will take a deliberate approach to documenting where we don't understand our systems. This includes behaviour (i.e. what it does), performance (i.e. how well it works) and automation (i.e. making it self-documenting).

### 4 — Develop in the open

All new source code is open. As we migrate from legacy, a larger proportion will become open. 

We should be proud of our engineering process and build in the open.  We should aim to explain our system architecture to anyone in the community who's interested. We should also aim to have any new member of staff, contractor, or developer at a partner organisation able to get up to speed given existing documentation.

The engineering documentation will talk about things like code quality, programming languages and system architecture. Just like our 'transparent operations', it is a natural counterpart to our overall organisational strategy.

### 5 — Prototype, support and build out new features
We will collaborate with the Product team to identifies new features. This includes discovery and prototyping to discover feasibility, where that helps.

We should tune our development processes for early feedback from users and make sure that we can incorporate that feedback into the evolution of our system. 
 
### 6 — Embrace the Research Nexus
The [Research Nexus](https://www.crossref.org/documentation/research-nexus/) gives us an exciting new framing for our metadata formats. We should embrace the design principles of the Research Nexus in all functionality. This includes, for example, integrating with other identifier systems.


## System Goals

### 1 -- Serve the Research Nexus

1. Model domain objects (e.g. articles, members, authors, organizations) in a way that can be represented in the Item Graph.
2. Don't create custom structures and processes for objects unless there's a very good reason.

### 2 -- Long-term

 1. Know the difference between core functionality (which must be stable) and non-core (e.g. specific schemas and business rules). 
 2. Choose stable, proven, technology for core functionality. 
 3. Choose innovative technology to move faster on non-core features.

### 3 -- Support both transactional and metadata representation

We're building a system that models two very different aims:

 1. Storing bibliographic metadata in an open format. Our community has visibility into this.
 2. Behaviour relating to that metadata, e.g. content registration, billing, notifications as well. Our users are stakeholders in this. 

We must strike the balance between API functionality that does a specific job and stable metadata representation. 

### 4 -- Easy for users to work with, at all levels of technical ability.

1. APIs should be self-documenting
2. APIs should have sensible defualts.
3. We provide user interfaces that are well designed and usable.

### 5 -- Wholesale

Allow integration with our system at all stages of the chain. Our users are diverse, and we should enable them to integrate in a way that meets their own needs. That includes different models of service providers and sponsorship, different objectives, different human languages and different programming langauges.

1. If we provide special-purpose APIs, expose the underlying data so that someone else could do the same thing.
2. If we provide User Interfaces, expose the same data so that someone else could run their own. 