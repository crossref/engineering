---
title: "Glossary"
linkTitle: "Glossary"
weight: 120
description: >
   Important domain and technical terms
tags:
- TODO
---

| | |
|-|-|
| CS | See Content System |
 Content System | Legacy Java codebase. |

## Community, membership, auth

| | | 
|-|-|
| Legacy role | TODO |
| Legacy credential | In the [support documentation](https://www.crossref.org/documentation/member-setup/account-credentials/) this is described as "Organization-wide shared role credentials".|
| Owner prefix | TODO |
| Email Credential | A credential (i.e. means of authorizing a user of the system). In the [support documentation](https://www.crossref.org/documentation/member-setup/account-credentials/) this is described as 'personal user credential'. |
| API Token | TODO |
| API Key | TODO |
| Organization | TODO |

## Metadata
| | | 
|-|-|
| Content Negotiation | The same concept as in [HTTP Content Negotiation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Content_negotiation). Our REST API has content-negotiation functionality for scholarly metadata, and exposes a variety of data formats. See {{< link "crosscutting/content-negotiation" >}}. |
| Identifier | A URI or URL that identifies a specific resource. Multiple Identifiers may refer to the same resource. 