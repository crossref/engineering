---
title: "Event Data Agents"
linkTitle: ""
weight: 20
description: Agents for harvesting Event Data from sources across the web.
tags:
 - clojure
 - event-data
extUrls:
 - https://gitlab.com/crossref/event_data_agents
---






