---
title: "Cayenne"
linkTitle: "Cayenne"
weight: 10
description: >
   Cayenne powers most of version 1 of the REST API, typically routes starting `api.crossref.org/v1/`
tags:
 - clojure
 - unixsd
 - citeproc-json
extUrls:
 - "https://gitlab.org/crossref/rest_api"
---

Cayenne is responsible for ingesting, indexing UniXSD documents from Content System into Elastic Search. It then serves up queries. Cayenne also ingests data from other places, such as MemberInfo, ASJC subject codes, etc. Cayenne also performs Content Negotiation to a variety of metadata formats.