---
title: "Search-Based Reference Matcher SBMV"
linkTitle: ""
weight: 30
description: >
  Java implementation of a common reference matching component based on the search API.
tags:
 - java
extUrls:
 - https://gitlab.com/crossref/search_based_reference_matcher
---






