---
title: "Console Frontend"
linkTitle: "Console Frontend"
weight: 10
description: Frontend repository for new user interfaces
tags:
 - vuejs
 - typescript
extUrls:
 - https://gitlab.com/crossref/console
---

Currently serving Key Manager, Content Registration User Interface

