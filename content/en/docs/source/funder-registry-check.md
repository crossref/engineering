---
title: "Funder Registry Check"
weight: 10
description: Tool to check for common errors in the Funder Registry RDF file prior to ingestion.
tags:
 - kotlin
 - funder-registry
extUrls:
 - https://gitlab.com/crossref/tools/funder-registry-check
---

This is used as part of {{< link "tools-and-processes/funder-registry-ingestion" >}}

