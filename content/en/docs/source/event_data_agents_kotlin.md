---
title: "Event Data Agents (Kotlin)"
linkTitle: "Event Data Agents (Kotlin)"
weight: 20
description: >
  Base for Event Data Agents in Kotlin. 
tags:
 - kotlin
 - event-data
extUrls:
 - https://gitlab.com/crossref/event_data_agents_kotlin
---






