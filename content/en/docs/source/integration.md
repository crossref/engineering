---
title: "Integration Environment"
linkTitle: "Integration Environment"
weight: 10
description: >
  Containerized environment that runs our entire greenfield system and contains our end-to-end test suite.
tags:
 - 
extUrls:
 - https://gitlab.com/crossref/integration
---
