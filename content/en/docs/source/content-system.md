---
title: "Legacy Content System (Closed Source)"
linkTitle: "Content System"
weight: 100
type: docs
description: >
   Content System powers the deposit system, XML queries, admin console and many other functions. It serves most endpoints on `doi.crossref.org`.
tags:
 - java
extUrls:
 - https://gitlab.com/crossref/content_system
---

Unfortunately we're not able to open-source the legacy Content System codebase but we're working hard on greenfield code.