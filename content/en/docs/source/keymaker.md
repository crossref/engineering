---
title: "Keymaker"
linkTitle: "Keymaker"
weight: 10
description: Administer API keys which can be used to access Crossref services and APIs without requiring usernames or passwords. The underlying authentication and identity provision is powered by Keycloak.
tags:
 - kotlin
extUrls:
 - https://gitlab.com/crossref/keymaker
---






