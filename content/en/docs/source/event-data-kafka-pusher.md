---
title: "Event Data Kafka Pusher"
linkTitle: ""
weight: 20
description: Push Events to the Event Bus from Kafka.
tags:
 - clojure
 - event-data
extUrls:
 - https://gitlab.com/crossref/event_data_kafka_pusher
---






