---
title: Source Code
linkTitle: Source
menu:
  main:
    weight: 50
---

The Crossref system is powered by a large number of source code repositories. For a full list, see [Crossref's Gitlab account](https://gitlab.com/crossref). 

Here are the significant codebases that comprise our production code. See {{< link "building-blocks/languages" >}} for a discussion of our choices.

For a full view of how source code repositories map to URLs, see the deployment view.

