---
title: "Event Data Percolator"
linkTitle: ""
weight: 20
description: Service to identify, filter and extract Events from inputs fed to it by Agents as part of Crossref Event Data.
tags:
 - clojure
 - event-data
extUrls:
 - https://gitlab.com/crossref/event_data_percolator
---






