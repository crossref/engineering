---
title: "Manifold"
linkTitle: "Manifold"
weight: 10
type: docs
description: >
   Manifold powers most of version 3 of the Crossref REST API, typically routes starting `api.crossref.org/v3/`
tags:
 - kotlin
extUrls:
 - https://gitlab.com/crossref/manifold
 - https://crossref.gitlab.io/manifold
---

