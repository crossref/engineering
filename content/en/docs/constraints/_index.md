---
title: "Constraints"
linkTitle: "Constraints"
weight: 20
tags:
- TODO
draft: true
---

Software Design Challenges
Crossref poses some interesting software engineering challenges. There are different ways one could approach software development for Crossref, but here are the factors that have cropped up. These are aimed at someone with a conventional software background but no familiarity with Crossref.
Domain Modelling — Internal and External
The domain model is the reason the technology is here. It is how we represent our community and reflect it back to itself. We have progressively built it out over the past two decades.

We have two principal external representations of the domain model in Ubiquitous Language:
the XML schema which is used for accepting deposits and serving queries
the REST API JSON schema which is used for serving queries.

These two models encompass the same domain objects (e.g. journals, articles, books, authors, funders, members and much more besides) but represent them slightly differently.

Having two concurrent expressions of the domain model is not necessarily a problem, it just requires us takes care to maintain both at once and consider all changes from both directions. The affordances of JSON and XML must be considered when making any change to the model, else we could end up with some representation problems. 

We have two families of internal domain model: 

The model implicitly encoded in the Content System codebase and SQL. This is not tested or documented. 
The Item Tree model present in Cayenne, which has a full regression test suite for parsing and data generation. 

The first model has been built incrementally over 20 years. The Item Tree model has been built and refactored over the past 10 years. 

It is beneficial to have an internal and external data model, as it allows for flexibility and variety of external formats. 

For this reason we chose to base Manifold on this internal Item Tree data model, and incorporated the parser from Cayenne because it's been used as the public-facing pipeline for a decade. In the Manifold codebase we generate outputs, including Journal Article XML, with a full test suite.
Domain Modelling — Longevity
When we publish a schema for public use, we commit to maintaining support for it. We support a number of previous of the deposit schema for deposits. Even if we drop support for depositing in a particular schema, we keep all historical XML files in historical schema formats. 

This means it's next to impossible to remove features. 

All code that interacts with XML is required to be backward-compatible in perpetuity. This means that e.g. the Cayenne XML parser must accommodate this.

It also means that 
Domain Model — Coupling 
this makes decomposition interesting. 


They also couple domain objects which makes it challenging to introduce bounded contexts. For example, the concept of members is present in articles, and vice versa.

The architecture of Manifold is designed to enable this. f

Fundamentally, any software design must 


The Metadata Schema is a public shared vocabulary. It couples together multiple domains (books, articles, funders, members, authors). This makes decomposition interesting. 

data on file.

Crossref operates at scale.


Behaviour
