---
title: "Tools"
---

- [Visual Studio code](https://code.visualstudio.com/) for general editing.
- [IntelliJ IDEA](https://www.jetbrains.com/idea/) for Kotlin. The community version suits our needs. 
- [Commitizen](https://commitizen-tools.github.io/commitizen/) for commit messages from the command line. All commits to git must be in conventional commit format.

## Docker

Don't use Docker Desktop, we are not licensed for it. There are various good alternatives:

 - [Colima](https://github.com/abiosoft/colima) 
 - [Multipass](https://multipass.run/) has a docker image

## Dependencies

- [Postgres.app](https://postgresapp.com/) for simple running and management of PostgreSQL on Mac OS.
- [LocalStack](https://www.localstack.cloud/) run AWS dependencies (SQS, S3, etc) in docker for local development.

## Visual Studio Extensions

 - Cucumber `cucumberopen.cucumber-official` 
 - JSON Schema Diagnostics `prosser.json-schema-2020-validation`. Useful for debugging JSON Schemas.

## IntelliJ IDEA Plugins

 - Cucumber for Java. Good for running and debugging BDD Feature files.
 - SonarLint. Always keep an eye on this. See {{<link "quality-requirements/code-quality" >}}.


## Other

 - [Draw.io](https://www.drawio.com/) - Create PNGs and SVGs with embedded data for re-editable images. As used on this site.

 