---
title: "How to implement item tree refactors in the Cayenne code"
authors:
 - "Joe Wass"
tags:
 - cayenne
 - item-tree
---

Sometimes we need to refactor the Item Tree data model without changing inputs and outputs. The input (XML) will remain the same and the output (Citeproc-JSON) will not change. 

What we will change is the XML parser, the item tree model it produces, and the converter back into Citeproc-JSON (wrapped in some Elastic Search specific fields).

We have three separate test suites that have pairs of input and expected output files. The 'parser' test suite ingests XML and produces an Item Tree in EDN format. The 'convert' test suite takes the same EDN item trees and a JSON Citeproc-JSON Elastic format.

These tests suites are not connected, to allow them to have different test cases where needed. 

Here's how to make these changes:

1. Make the desired change in the parser. e.g. in `cayenne.formats.unixsd` and/or `cayenne.formats.unixref` .
2. Run tests to verify that they break
    1. `lein test cayenne.formats.parser-regression-test`
3. Re-generate the XML -> Item Tree regression test files:
    1. `rm dev-resources/parser-regression/*.edn`
    2. `lein repl`
    3. `(load "/cayenne/formats/parser_regression_test")`
    4. `(in-ns 'cayenne.formats.parser-regression-test)`
    5. `(generate-result-files)`
5. This will re-generate the files. Very carefully diff them to see if it's what you expect.
6. Run tests to verify that they now all pass
    1. `lein test :unit`
7. If so, move on to the converter. First replace the input files for regression testing. 
    1. `cp dev-resources/parser-regression/*.edn dev-resources/convert-regression/`
    2. Note that there are some files that are present in `convert-regression` that are not in `parser-regression` so you may need to tweak some item trees.
8. Run tests to verify that they break.
    1. `lein test :only cayenne.elastic.convert-regression-test`
8. Make the changes to the code that converts from item tree to elastic-search JSON docs. e.g. `cayenne.elastic.convert`
9. If you're making a non-breaking change you should not have to change the output files at all! If, for some reason you do:
    1. `(load "/cayenne/elastic/convert_regression_test")`
    2. `(in-ns 'cayenne.elastic.convert-regression-test)`
    3. `(generate-result-files)`
10. You may want to review `cayenne.works.retrieval-test`. 

