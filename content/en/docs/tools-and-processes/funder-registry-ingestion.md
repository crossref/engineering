---
title: Funder Registry Ingestion
tags:
 - funder-registry
---

This is a manual process. Some steps are performed by a developer, some by a product owner. The steps are detailed here:

https://docs.google.com/document/d/1zQDZJt3uI8cvh24rUHgEGYQ55ly-q9aI07SxArvA0WQ/edit

The {{< link "source/funder-registry-check" >}} tool is used.
