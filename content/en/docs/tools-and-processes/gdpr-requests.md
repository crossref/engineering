---
title: Handling GDPR requests
tags:
 - GDPR
 - authenticator
 - keycloak
 - content_system
 - sugar
---

Requests to remove data from our systems are handled manually. The following checklist describes how to find and remove emails from users that request it.

**Authenticator**

 - Login to [authenticator](https://authenticator.crossref.org/admin/login/?next=/admin/)
 - Go to users
 - Search the given user (email), see if it is found on the list

**Keycloak**
 - TODO detailed steps to delete user in Keycloak

If the user is found on Authenticator and Keycloak, it can be deleted from Authenticator. There is synchronization to Keycloak, but you should check to be sure that the user gets actually deleted from Keycloak.

**Sugar**

Removing the user from sugar and other 3rd part systems is out of scope from this list. However, knowing if the user was registered on sugar helps us rule out several parts of content_system that integrate with Sugar.


**Content System**

Regardless of the user being or not being in Authenticator + Keycloak, we should check content_system, as some reports use email to notify the users. 
The process is documented [here](https://docs.google.com/document/d/1i6l1nCGEdqhHdN3MhMBh1bOWnPp4FhR9KKacdz_WlKo/edit?usp=sharing)

