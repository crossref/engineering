---
title: Manual Deployment of Content System
---

## Planning releases
 - In most cases each feature branch will result in a change in the version number and a deployment. 
 - Release notes should be based around the feature issue or issues that contributed to the release. This will have been agreed with the product owner in advance.
 - Like all greenfield development artifacts, release notes will be public. Some of our users are interested in digging through our code and releases, so don't assume too much internal knowledge to interpret. 
 - A release will be tagged on the master branch of the project. Work should be planned and sequenced so that it's merged to the develop branch, then to master, in a releasable order. Making a number of smaller releases, and regularly rebasing feature branches, keeps this flexible.
 - Where necessary, prior to a release, invite the product owner to smoke test.
 - If the change involves database migrations ensure that they succeed and the system still works as expected.
 - The deployment will be triggered with CI automatically from the tag. Perform adequate supervision.

## Performing a release of Content System

A new release is made by merging `develop` to `master`.

1. Go to the project and find the latest deployed tag version number. You'll refer to it later.
2. Create a new merge request from `develop` to `master`.
   1. Go to the Project page.
   2. Click 'Merge Reqests' -> 'New Merge Request'
3. Select `develop` as the source branch and `master` as the target branch.
4. Scroll to the bottom of the merge request and review the diffs. 
   1. The 'Changes' tab shows diffs are being introduced.
   2. The 'Commits' tab shows issues addressed. Commits should link to issue numbers.
5. Decide on a new version number. 
   - It should be of the format `vMAJOR.MINOR.PATCH`.
   - We use [Semantic Versioning](https://semver.org/). This isn't suitable for all of our projects but it's beneficial to use the same scheme across our codebases. Most of the time we will be making a backwards-compatible change, so the MAJOR number (middle) will be incremented.
   - You will normally increment the middle number, e.g. `v0.100.4` would become `v0.101.0`.
6. Scroll to the top and name your merge request after the version number, e.g. "v0.101.0".
7. Write a one-line description of this release in the description.
8. Write a bullet-pointed list of the changes that have gone into the release.
   - Reference issues with the Jira issue number (e.g. `CR-1234`) so they can be linked back.  
   - Browse back at previous releases for inspiration. There should be continuity of content and style.
9. The notes should allow:
   - Users outside Crossref to understand what changes they might expect from release.
   - Support to identify changes that could explain a change in behaviour.
   - Product managers to know at which point a given user story or fix was released.
   - Support to know which features went into production at a given point in time.
10. Save the merge request.
11. One final proof-read, feel free to ask a colleague to review although it's not required as all changes should have been code-reviewed before merging to develop.
12. Wait for the build and test pipeline to go green. If it doesn't, stop and review.
13. Click 'merge'. Do not 'squash' commits (each merge request to **into** develop could have squashed)
14. Now master is up to date, create a tag.
   1. Note the latest commit hash that was merged to master.
   2. Copy the text of the merge request for later.
   3. Click 'repository' -> 'tags'.
   4. Click "New Tag" and give your tag the same title.
   5. Select "Create from `master`"
15. Double-check that the tag represents the correct commit hash. In the past there has been a race condition where the latest version of the branch didn't end up being comitted.
16. Now tag is ready, so create a release
   1. Click 'Deployments' -> 'Releases'.
   2. Click 'New Release' and select the Tag you just created. Make sure the release title matches the tag.
   3. Copy the description into the Release Notes field.
   4. Click 'Create Release' and the release jobs will start.
17. The compiled output (e.g. WAR file) will be built by CI with the appropriate version number. 
18. Follow the CI jobs through as there are steps that currently need to be kicked off manually (stop queue, deploy, start queue).

## Performing a release of Authenticator

The deployment for authenticator must be done within the 2 hour window reserved for CS release, make sure the branch you want to release has been merged into `master` and has passed all the tests. It has been pushed to the docker registry and it is already available and tested on `staging`. After merging it, you will find a new pipeline labeled as `main` in https://gitlab.com/crossref/authenticator/-/pipelines 

The release for authenticator should be done just before CS is released, making sure that if something goes wrong there is time to roll back and fix any issues. Database backup will not be necessary if there are no migrations or data changes. Generally there is a daily backup but it will need to be assessed whether it is worth making a DB backup just before the release process. 

In order to release a new version of authenticator you need to:

1. go to https://gitlab.com/crossref/authenticator/-/pipelines and click on the latest pipeline labeled as `main`
2. You will see that all steps have been run except `deploy-production` and `deploy-sandbox`
3. as a test you can deploy sandbox first, but ideally that should be done outside of the release window.
4. Press `deploy-production` play button. That will take few minutes to deploy
5. to check if it is deployed you can check that the new version is present on https://authenticator.crossref.org/heartbeat/
6. When the release has finished it is time to test the authenticator and systems for any malfunctioning (primarily CS)
7. In case of finding any issues you can roll back to the previous release by looking for the previous pipeline labeled as `main` in https://gitlab.com/crossref/authenticator/-/pipelines 
8. Then press the play button on the `deploy-production` step. Again it might take several minutes.
9. If a dabase dump is required as well, step 8 needs to be synced with devops team which will indicate the right time for the rollback release.