---
title: "Small Apps Deployment"
linkTitle: "Small Apps Deployment"
tags:
  - small_apps
---

## Intro

Small apps come in 2 kinds:

1. Web apps, they are user-facing and mostly display reports, metrics, and other meta information.
    - They're run on tomcats on `bstn-prod-tcat-01`.
    - Each web app has its own tomcat and its own service in `systemctl`.
    - Env vars are stored in `/var/lib/tomcat/ansible_role_name/bin/setenv.sh`

2. Cron apps, they run on schedule. They mostly produce the reports and metrics that web apps display, by collecting
   info from production DBs and storing the results in MySQL DBs.
    - Each app has its own run script which is scheduled via cron on `bstn-prod-tcat-01`.
    - Launch times in cron are staggered to not exhaust the server's CPU and RAM.
    - Env vars are stored in `/home/crossref/ansible_role_name/setenv.sh`

Every app has an individual `role` in the Ansible repo which contains run scripts, env vars, and cron schedules that this app
needs.

## List of apps

| Colloquial name                  | Type                                    | Source                                                                       | Ansible role                                                                                                              |
|----------------------------------|-----------------------------------------|------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| Conflict Report                  | <span style="color:#468847">cron</span> | [conflict_report](https://gitlab.com/crossref/conflict_report)               | [conflictreport](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_conflictreport)           |
| Customer DB reports              | <span style="color:#7980FF">web</span>  | [customer_db_reports](https://gitlab.com/crossref/customer_db_reports)       | [customerdbreports](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_customerdbreports)     |
| Deposit report (journals)        | <span style="color:#468847">cron</span> | [deposit_report](https://gitlab.com/crossref/deposit_report)                 | [depositreport](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_depositreport)             |
| Deposit report (books and confs) | <span style="color:#468847">cron</span> | [deposit_report_book](https://gitlab.com/crossref/deposit_report_book)       | [depositreportbook](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_depositreportbook)     |
| DOI Complaint webform            | <span style="color:#7980FF">web</span>  | [doi_error_catcher](https://gitlab.com/crossref/doi_error_catcher)           | [doicomplaint](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_doicomplaint)               |
| DOI Error Checker                | <span style="color:#468847">cron</span> | [doi_error_db_processor](https://gitlab.com/crossref/doi_error_db_processor) | [doierrorchecker](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_doierrorchecker)         |
| ExcludeList                      | <span style="color:#7980FF">web</span>  | [exclude_list](https://gitlab.com/crossref/exclude_list)                     | [excludelist](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_excludelist)                 |
| GetMdByTitle                     | <span style="color:#468847">cron</span> | [getmdbytitle](https://gitlab.com/crossref/getmdbytitle)                     | [getmdbytitle](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_getmdbytitle)               |
| GetMdStats                       | <span style="color:#468847">cron</span> | [get_md_stats](https://gitlab.com/crossref/get_md_stats)                     | [getmdstats](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_getmdstats)                   |
| GoLive Report                    | <span style="color:#468847">cron</span> | [golive-status-report](https://gitlab.com/crossref/golive-status-report)     | [golivereport](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_golivereport)               |
| IgnoreFields                     | <span style="color:#7980FF">web</span>  | [ignore_fields](https://gitlab.com/crossref/ignore_fields)                   | [ignorefields](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_ignorefields)               |
| MyCrossref                       | <span style="color:#7980FF">web</span>  | [old_mycrossref](https://gitlab.com/crossref/old_mycrossref)                 | [mycrossref](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_mycrossref)                   |
| Resolution Report                | <span style="color:#468847">cron</span> | [resolution_report](https://gitlab.com/crossref/resolution_report)           | [resolutionreport](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_resolutionreport)       |
| Schematron Processor             | <span style="color:#468847">cron</span> | [schematron_processor](https://gitlab.com/crossref/schematron_processor)     | [schematronprocessor](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_schematronprocessor) |
| TitleDbBuilder                   | <span style="color:#468847">cron</span> | [title-db-builder](https://gitlab.com/crossref/title-db-builder)             | [titledbbuilder](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_titledbbuilder)           |
| TitleList                        | <span style="color:#7980FF">web</span>  | [title-list](https://gitlab.com/crossref/title-list)                         | [titlelist](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_titlelist)                     |
| Web Deposit                      | <span style="color:#7980FF">web</span>  | [web_deposit](https://gitlab.com/crossref/web_deposit)                       | [webdeposit](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_webdeposit)                   |
| XSDParse                         | <span style="color:#7980FF">web</span>  | [xsd_parse](https://gitlab.com/crossref/xsd_parse)                           | [xsdparse](https://gitlab.com/crossref/infrastructure/ansible/-/tree/main/roles/smallapps_xsdparse)                       |

## Web Apps Deployment
1. Go to the `Releases` page in Gitlab (for example, https://gitlab.com/crossref/web_deposit/-/releases)
2. Download the `.war` file from the release you want to deploy (i.e. `webDeposit.war`)
3. Copy the app from your system to your home dir on the production server:

   ```rsync -ahzP "./webDeposit.war" your_ssh_login@bstn-prod-tcat-01:/home/your_ssh_login/webDeposit.war```
   
4. SSH into the production server: `ssh bstn-prod-tcat-01`

<span style="color:red">WARNING!</span> Steps below change the production app config or binary. Make sure to have backups, and check the new version immediately after deploy.

5. Copy the new app version to its tomcat: `sudo cp /home/your_ssh_login/webDeposit.war /var/lib/tomcat/webdeposit/webapps/webDeposit.war`
6. Restart the app tomcat: `sudo systemctl restart tomcat@webdeposit`

## Cron Apps Deployment
1. Go to the `Releases` page in Gitlab (for example, https://gitlab.com/crossref/title-db-builder/-/releases)
2. Download the `*-jar-with-dependencies.jar` file from the release you want to deploy (i.e. `titledbbuilder-jar-with-dependencies.jar`)
3. Copy the app from your system to your home dir on the production server:

   ```rsync -ahzP "./titledbbuilder-jar-with-dependencies.jar" your_ssh_login@bstn-prod-tcat-01:/home/your_ssh_login/titledbbuilder.jar```
   
4. SSH into the production server: `ssh bstn-prod-tcat-01`

<span style="color:red">WARNING!</span> Steps below change the production app config or binary, make sure to have backups.

5. Copy the new app version to its directory: `sudo cp /home/your_ssh_login/titledbbuilder.jar /home/crossref/titledbbuilder/titledbbuilder.jar`

The app will run the new version at the next scheduled time.

To run it manually on the server:
1. Switch user to crossref: `sudo su - crossref`
2. Run the app: `LOGS_DIR=/home/crossref/logs /home/crossref/titledbbuilder/titledbbuilder.sh` (you need to set the LOGS_DIR manually because usually it's defined by the cron task)