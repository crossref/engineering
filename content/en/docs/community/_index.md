---
title: Open Source Community
description: >
  Our place in the Open Source community.
---

## JSON Forms Vuetify renderers

We contributed to the JSON Forms Vuetify renderers library. 
See [jsonforms-vuetify-renderers on GitHub](https://github.com/eclipsesource/jsonforms-vuetify-renderers).

## API Clients

We collaborated with Scott Chamberlain on some API client libraries. See [Scott's post](https://www.crossref.org/blog/python-and-ruby-libraries-for-accessing-the-crossref-api/) on our blog.

- [Habanero API client in Python](https://github.com/sckott/habanero)
- [Serrano API client in Ruby](https://github.com/sckott/serrano)

