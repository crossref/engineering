---
title: Issue Tracker
linkTitle: Issues
menu:
  main:
    weight: 20

---

We use Jira for issue tracking and planning. All issues except those containing confidential information are open. **You do not need a Jira account to browse or submit issues.** 

<a class="btn btn-lg btn-primary mr-3 mb-4" href="https://crossref.atlassian.net/jira/software/c/projects/CR/issues/">
		Crossref Issue Tracker on Jira <i class="fas fa-arrow-alt-circle-right ml-2"></i>
</a>

If you want to discuss anything, please had over to our [Community Forum]({{< ref "/community" >}})