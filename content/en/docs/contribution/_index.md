---
title: "Contribution Guidelines"
linkTitle: "Contribution Guidelines"
weight: 500
description: >
  How to contribute to Crossref's code
tags:
---

## Welcome

We operate open infrastructure for the scholarly community, and welcome the community's involvement in building that infrastructure. We would love to hear from you if you want to get involved.

Our aim is to make our greenfield code easy to run on any devloper's computer, and therefore easy to contribute to and test. 

We will consider any merge request on any of our respositories (see {{<link "source/_index" >}}) that meet our code quality standards (see {{<link "quality-requirements/_index">}}). Feel free to contact us on the [community forum](https://community.crossref.org), raise an [issue]({{< ref "/docs/source" >}}) or contact Joe Wass@crossref.org directly if you want to discuss.

## License

Our code is all MIT licensed. Community code contributions will be licensed as such.

## Contributor Code of Conduct

As contributors and maintainers of this project, we pledge to respect all people who
contribute through reporting issues, posting feature requests, updating documentation,
submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free experience for
everyone, regardless of level of experience, gender, gender identity and expression,
sexual orientation, disability, personal appearance, body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual language or
imagery, derogatory comments or personal attacks, trolling, public or private harassment,
insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject comments,
commits, code, wiki edits, issues, and other contributions that are not aligned to this
Code of Conduct. Project maintainers who do not follow the Code of Conduct may be removed
from the project team.

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by
opening an issue or contacting one or more of the project maintainers.

This Code of Conduct is adapted from the Contributor Covenant
(http:contributor-covenant.org), version 1.0.0, available at
http://contributor-covenant.org/version/1/0/0/
