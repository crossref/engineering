---
title: "Registered Content Metadata"
tags: 
- architecture-diagram
---

This is the metadata that our members send us, we process and expose in our APIs. 

![Overview](./metadata.drawio.svg)

## See also
 - {{< link "crosscutting/matching" >}}
 - {{< link "building-blocks/deposit/_index" >}}

See the [architecture-diagram]({{< ref "/tags/architecture-diagram" >}}) tag for other diagrams.
