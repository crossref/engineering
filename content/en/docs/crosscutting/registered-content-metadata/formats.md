---
title: Registered Content Metadata Formats
---

Registered Content Metadata is modelled in the following formats:

 - XML (see {{< link "crosscutting/registered-content-metadata/xml-formats" >}})
 - JSON (see {{< link "crosscutting/registered-content-metadata/json-formats" >}})
 - Item Tree