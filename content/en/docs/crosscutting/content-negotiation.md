---
title: "Content Negotiation"
authors: 
 - Joe Wass
tags:
- rest-api
- content-negotiation
---

See {{< dr "DR-0100" >}}.

Content Negotiation is a standard concept from HTTP. See the [MDN Content Neogtiation page](https://developer.mozilla.org/en-US/docs/Web/HTTP/Content_negotiation) and the [Wikipedia article](https://en.wikipedia.org/wiki/Content_negotiation). It allows a client (web browser, or software run by the user) to request a particular format. The original use for this functionality is for users to specify what formats they can accept (e.g. GIF but not JPG) so the server can send an appropriate format result. 

## Content Negotiation on the REST API

There is a set of content types for scholarly metadata that Crossref supports:

 - application/rdf+xml
 - text/turtle
 - application/vnd.citationstyles.csl+json
 - text/x-bibliography
 - application/x-research-info-systems
 - application/x-bibtex
 - application/vnd.crossref.unixref+xml
 - application/vnd.crossref.unixsd+xml

When we talk about Content Negotiation we generally mean one of these formats. To perform content negotiation the user requests a URL and sets the `Accept` header to the requested content type. 

The Crossref REST API provides a special endpoint that is capable of content negotiation, which is reached by appending `/transform` to the end of a `/work` endpoint:

```
http://api.crossref.org/v1/works/«DOI»/transform
```

e.g.

```
http://api.crossref.org/v1/works/10.5555/12345678/transform
```

If you visit that URL you will likely get an HTTP 406 "No acceptable resource available." because your browser requested a format that is incompatible with the metadata. E.g. a web browser might send

```
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
```

If you request a content the that REST API can provide then you will get a reply. E.g. the following CURL command:

```
% curl -H "Accept: text/x-bibliography" "http://api.crossref.org/v1/works/10.5555/12345678/transform"
Carberry, J. (2008). Toward a Unified Theory of High-Energy Metaphysics: Silly String Theory. Journal of Psychoceramics, 5(11), 1–3. CLOCKSS. https://doi.org/10.5555/12345678
```

## Content Negotiation for DOIs

### Crosscite
The DOI resolution service is provided by the DOI proxy and works by redirecting users to endpoints. By default when you resolve a DOI you are sent to the landing page for the resource. But if you supply a relevant Accept header, and the DOI is registered for Content Negotiation, the DOI Proxy will direct you to the relevant content negotiation endpoint. This pattern is applied across a number of DOI registration agencies, and is called "CrossCite". See the [CrossCite documentation page](https://citation.crosscite.org/docs.html) for full details.

### Redirection

The DOI proxy always redirects users when you resolve a DOI. To the landing page under normal use. To the content negotiation API in content negotiation.

An example using the above DOI:

```text {linenos=true}
% curl -v -L  -H "Accept: text/x-bibliography" "http://doi.org/10.5555/12345678"
...
> GET /10.5555/12345678 HTTP/2
> Host: doi.org
> Accept: text/x-bibliography
>
...
< HTTP/2 302
< location: https://api.crossref.org/v1/works/10.5555%2F12345678/transform
....
> GET /v1/works/10.5555%2F12345678/transform HTTP/1.1
...
> Accept: text/x-bibliography
....
< HTTP/1.1 200 OK
< content-type: text/x-bibliography
...
Carberry, J. (2008). Toward a Unified Theory of High-Energy Metaphysics: Silly String Theory. Journal of Psychoceramics, 5(11), 1–3. CLOCKSS. https://doi.org/10.5555/12345678
```

Note the `doi.org` server uses a `Location` header and an HTTP 302 status code to redirect to the REST API endpoint. 

Also note that the client sends the `Accept` header to the first request, and then to the subsequent request as a result of the HTTP 302 redirect. The `Accept` value is used twice:

1. First by `doi.org` to detect that we are doing content negotiation.
2. Then by `api.crossref.org` to detect the actual content type being requested. 

### Registering a DOI for Content Negotiation

Not all DOIs support content negotiation. 

A DOI is intrinsically linked to naming authority via its prefix. E.g. `10.5555/12345678` is linked to `0.na/10.5555`. 

Content Negotiation 

A look `0.na/10.5555` shows that its `HS_SERV` value is `10.SERV/CROSSREF`. <https://hdl.handle.net/api/handles/0.na/10.5555>

```json
{
  "responseCode": 1,
  "handle": "0.na/10.5555",
  "values": [
    {
      "index": 1,
      "type": "HS_SERV",
      "data": {
        "format": "string",
        "value": "10.SERV/CROSSREF"
      },
      "ttl": 86400,
      "timestamp": "2023-04-12T02:14:02Z"
    },
...
  ]
}
```

Looking at the `10.SERV/CROSSREF` we see a `10320/loc` entry:

```json
{
  "responseCode": 1,
  "handle": "10.SERV/CROSSREF",
  "values": [
...
    {
      "index": 4,
      "type": "10320/loc",
      "data": {
        "format": "string",
        "value": "<locations http_sc=\"302\">\n<location weight=\"0\" http_role=\"conneg\" href_template=\"https://api.crossref.org/v1/works/{hdl}/transform\" />\n</locations>\n"
      },
      "ttl": 86400,
      "timestamp": "2021-10-13T18:48:17Z"
    },
...
  ]
}
```

Here we see the configuration for content negotiations on Crossref DOIS (i.e. DOIs whose prefix is homed with the `10.SERV/CROSSREF`). The DOI.org resolver has special functionality to interpret this. Other RAs have different configurations:

```json
{
  "responseCode": 1,
  "handle": "10.SERV/DATACITE",
  "values": [
...
    {
      "index": 500,
      "type": "10320/loc",
      "data": {
        "format": "string",
        "value": "<locations http_sc=\"302\">\n<location weight=\"0\" http_role=\"conneg\" href_template=\"https://data.crosscite.org/{hdl}\"/>\n</locations>"
      },
      "ttl": 86400,
      "timestamp": "2019-10-02T02:04:42Z"
    }
  ]
}
```

