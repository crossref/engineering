---
title: "Identifiers"
---


 - [Identifier types in Manifold codebase](https://crossref.gitlab.io/manifold/dokka/manifold/org.crossref.manifold.identifiers/-identifier-type/index.html)
 - [Identifier Parsers in Manifold codebase](https://crossref.gitlab.io/manifold/dokka/manifold/org.crossref.manifold.identifiers.parsers/index.html)

TODO