---
title: "Cross-cutting Concepts"
linkTitle: "Cross-cutting Concepts"
weight: 80   
---

{{% pageinfo %}}
**This section is very work-in-progress!**
{{% /pageinfo %}}

Cross-cutting concepts span across our system. In many cases a cross-cutting concept will be implemented in a number of codebases. See the {{< link "building-blocks/_index" >}} for specific components that make up the system. 