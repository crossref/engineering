---
title: Code quality
---

New Production code is written in Kotlin and TypeScript. Legacy code has been written in Java, Clojure, JavaScript, Ruby, Python and even a little Perl. 

## Static Analysis.

We use SONAR and validate on build. [List of projects on SONAR Cloud](https://sonarcloud.io/organizations/crossref/projects). If you start a new project, configure it to report to SONAR by making reference to a similar project.

IntelliJ has a free SONAR plugin which will help you avoid failures in CI.

## Style

For Kotlin we use IntelliJ's code formatter. It's not compulsory to run it, but it's good practice. 

## Code review

Style, static analysis and code coverage are handled automatically so the don't need to be part of the code review conversation. 

Code review should cover whether the correct architectural or design decisions were made, whether tests were appropriate, whether the whole acceptance critera were covered, etc. 

The following should be met before submitting for code review. Reviewer should also confirm as the first step of code review.

1. Code should *not* be merged if:
   1. Tests don't pass. 
   1. For Legacy codebases, SONAR results should not have got _worse_.
   1. For Greenfield codebases, SONAR results should be green.
   1. They are not linked to an issue number(s) in commit messages. Click it to make sure it works.
1. Review requirements in the linked issues / user stories. Make sure all functionality is represented by tests and implemented. 
1. If code needs fixing, make the change and resubmit:
   1. If the merge reduces the test coverage, remove unused code or refactor to write new tests.
   1. If there is behaviour that hasn't been specified to the point where a unit test can be written, work with the Product Owner to clarify requirements.
1. Check the 'code review' box on the relevant tickets. Review other developer actions on the ticket. Don't move to 'done' until all boxes are ticked.
