---
title: Source Control and Continuous Integration
tags:
 - git
---

## Branches

- The stable branch should be called `main` (not `master`, see {{< dr "DR-0260" >}}).
- Usually feature branches will branch off, and merge back into `main`.
- A feature branch should be named after the Jira issue number. E.g. `feature/CR-5555`. They can optionally include the feature title, e.g. `feature/CR-5555-add-email`. This enables Jira to link to the branch.
- All commits must be made using Conventional Commit format. Use [Commitizen](https://commitizen-tools.github.io/commitizen/) to make sure you get the format right.
- Aim to make them precise. If you need to do a small refactor before working on a bigger feature, commit that with the appropriate scope. Aim to merge these as soon as you can. 
- Avoid squash merges, so we have the full history of the changes. But if you do need to rewrite history on your feature branch before merging to `main` you can use `git commit --amend` and `git push origin branch --force` which will keep the commit history neat.

We rely on using [Semantic Release](https://semantic-release.gitbook.io/semantic-release/) to write release notes and tags. 

## Checking and gating quality

The build should break when the branch isn't in a deployable state. This will prevent it from being merged or deployed until it's ready.

 - Any failed tests should break the build for that branch.
 - Code coverage should be exported from test runs and included in the SONAR report.
 - SONAR should be triggered for feature and main branches.
 - If the SONAR quality gate is not met (static analysis of code coverage) the build should break.  

## Building and distributing images

 - Images should be built from the CI pipeline for the `develop` and `main` branches, and for tags.
 - Images should be pushed into the GitLab Docker repository for easy retrieval by developers.
 - Images should also be pushed into the AWS Docker repository for deployment in AWS.

