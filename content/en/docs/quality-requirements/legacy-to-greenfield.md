---
title: "Legacy to Greenfield"
---


1. We will gradually improve our Legacy codebase, but we won't aim to make any fundamental changes.
1. As we focus on services, we will extract the code or rewrite the service wholesale.
1. If the code is simply extracted it will be classed as Legacy, but we will be in a better position to improve it. We will have to be tolerant of service boundaries being a little inexact.
1. If it is re-written it will be classed as Greenfield.
