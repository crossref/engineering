---
title: Error reporting
---

We use [Sentry](https://sentry.io/organizations/crossref/issues/) for error reporting.


## Introduction

In order to track exceptions that may occur in out software in a production or staging environment we use [Sentry.io](https://sentry.io). Sentry provides libraries for most of the programming languages so that you can connect your software with a sentry client and it will send any exception that may occur directly to Sentry.io. 

In Sentry you can create your own projects and every project will have a DSN, which is a url with the following format: `{PROTOCOL}://{PUBLIC_KEY}:{SECRET_KEY}@{HOST}{PATH}/{PROJECT_ID}` . That is what you need to setup in your sentry client in order to send the error messages to the right project.

### Quick example 

#### Python [DOC](https://docs.sentry.io/clients/python/)

```python
from sentry_sdk import init, capture_message

init("https://mydsn@sentry.io/123")

capture_message("Hello World")  # Will create an event.

raise ValueError()  # Will also create an event.
```

#### Java [DOC](https://docs.sentry.io/platforms/java/)

```java
import io.sentry.Sentry;

Sentry.init(options -> {
  options.setDsn("https://examplePublicKey@o0.ingest.sentry.io/0");
});
```



### Configuration

When initialising Sentry, different parameters can be configured.

`environment`: indicates whether the error ocurred in `production`, `staging` or any other environments you might have.

`release`: Commonly used to specify git commit hash or tag.

`Tags`: to specify extra tags 

`servername`: Name of the server it is running on

And many more

## Environments

The way we are currently specifying environments is via environment variables. We expect (as it is how it is configured via terraform) that the docker container running on AWS ECS will contain an environment variable called `ENV`.

Your software should be able to specify the environment it is running on by reading the `ENV` environment variable, you can then configure your sentry client like that:

*application.properties*

```java
sentry.dsn=${SENTRY_DSN}
sentry.environment=${ENV:-dev}
sentry.name=MyGreatApp
sentry.release=@pom.version@
```

Then you can filter events in Sentry.io website by environment, tag, release, etc.
