---
title: Specification, Acceptance Criteria, Behaviour-Driven Develoment
tags:
 - wip
---

We follow the Behaviour Driven Design (BDD) process to specify the behaviour of all Greenfield software. The guide for this is ["Writing Great Specifications" by Kamil Nicieja](https://www.manning.com/books/writing-great-specifications), buy a copy!

Our system is made up of various subsystems. The implementation for a given set of features may be spread across various codebases. See TODO Living Documentation.

When we write BDD scenarios we should aim to make them readable as part of the wider Living Documentation for the whole system. They should be written in the langauge domain of the Product and guided by the Product Owner.

When we build features we should build small slices of minimum viable increment and then aim to increment. Don't try to build large features in one go.