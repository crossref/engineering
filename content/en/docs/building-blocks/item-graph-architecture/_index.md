---
title: Item Graph Architecture
tags:
 - architecture-diagram
---

![Overview](./itemgraph-architecture.drawio.svg)


See {{< link "crosscutting/item-graph-data-model/_index" >}} for an explanation of the data model.