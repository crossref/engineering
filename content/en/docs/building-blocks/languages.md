---
title: "Programming Languages"
weight: 50
description: Languages and frameworks for production code.
---

Languages are split into front-end and back-end. Our production back-end languages are all JVM-based, which is useful for code-sharing and accessing certain domain-specific libraries for tight dependencies, such as the [Handle Client](https://www.handle.net/client_download.html).

## TypeScript 

On the front-end we use TypeScript (see {{< dr "DR-0085" >}}), along with:

- Vue.js
- Vutify
- [JSON Forms Vuetify renderers](https://github.com/eclipsesource/jsonforms-vuetify-renderers), which we contribute to

We put all of our new front-end code into {{< link "source/my-crossref" >}}. 

See the [typescript]({{< ref "/tags/typescript" >}}) tag.

## Kotlin

Our new back-end services, such as {{<link "source/manifold" >}} are written in Kotlin, in Spring Boot (see {{< dr "DR-0120" >}}). We chose Kotlin because it is expressive, has great good performance, and targets the JVM, a stable back-end technology. It is also easy to learn, which strikes a good balance between long-term maintainability and accessibility.

The other benefit of the JVM is that we can re-use code between our software projects, giving us a migration plan from our legacy code. For example Manifold uses the XML parser loaded from Cayenne (see {{< dr "DR-0205" >}}). 

See the [kotlin]({{< ref "/tags/kotlin" >}}) tag.

## Clojure

Clojure is a LISP on the JVM and has a lot to recommend it. It expressive and has strong functional-programming and concurrency features. Because of this it was extensively in Crossref Labs, resulting in two substantial code-bases: {{<link "source/cayenne" >}} and {{<link "source/event-data-query-api" >}}.

For its superpowers, Clojure is niche. Its type system, though flexible, doesn't quite meet our needs for building long-term code. We are therefore not writing any new production codebases in Clojure.

See the [clojure]({{< ref "/tags/clojure" >}}) tag.

## Java

Our {{<link "source/content-system" >}} codebase is written in Java 1.8, using [Spring 3.2](https://docs.spring.io/spring-framework/docs/3.2.12.RELEASE/changelog.txt). We also have various internal tools written in Java.

See the [java]({{< ref "/tags/java" >}}) tag.
