---
title: UniXSD Renderer in Manifold
tags:
 - xml
 - unixsd
 - crm-items
 - manifold
---

This is in early development. It renders an Item Tree to UniXSD. 

This renderer is in contrast to the {{< link "building-blocks/unixsd/unixsd-view-content-system" >}} class which packages pre-build XML into the view. 

