---
title: UniXSD
tags:
 - unixsd
 - xml
---

The UniXSD format is produced by both legacy and new code. See {{< link "crosscutting/registered-content-metadata/formats" >}} for explanation on the concepts.
