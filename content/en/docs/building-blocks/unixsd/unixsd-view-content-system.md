---
title: UniXSD View in Legacy Content System
tags:
 - xml
 - unixsd
 - crm-items
 - legacy-cs
---

The UNIXSD view formats packages XML into the UniXSD format. This is served up in query responses and available via the REST API. e.g 
<https://api.crossref.org/works/10.5555/12345678/transform/application/vnd.crossref.unixsd+xml> .

It's implemented in [org.crossref.qs.view.UnixsdView](https://gitlab.com/crossref/content_system/-/blob/develop/java/org/crossref/qs/view/UnixsdView.java).

It adds the schema locations:

 - `http://doi.crossref.org/schemas/unixref1.1.xsd`
 - `http://doi.crossref.org/schemas/unixref1.0.xsd`
 
It adds adds CRM Items:

  - `publisher-name`
  - `prefix-name`
  - `member-id`
  - `citation-id`
  - `journal-id`
  - `book-id`
  - `series-id`
  - `deposit-timestamp`
  - `owner-prefix`
  - `prime-doi`
  - `last-update`
  - `created`
  - `citedby-count`




