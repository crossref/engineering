---
title: "Working with dependencies in Kotlin projects"
linkTitle: "kotlin-dependency-management"
weight: 50
description: Languages and frameworks for production code.
authors:
 - Dima Safonov
tags:
- kotlin
---
## Why update dependencies
Keeping dependencies up-to-date is important: otherwise, when a critical CVE is found in one of your deps, 
you may realize that you're two major versions behind, and updating it will require an emergency code refactoring (which is risky by itself).

It's easier to amortize this update-and-refactor churn over the long period of time, when nothing is on fire.

## Build system
We use [Maven](https://maven.apache.org/) as our build and test system.

Maven configuration is stored in the `pom.xml` file in the project dir. 
`pom.xml` contains the project meta-data (such as name and version), a list of its dependencies, and plugins configuration.
Syntax reference: https://maven.apache.org/pom.html

## Tools and processes
### Finding unused dependencies
Run from the project directory:

`mvn dependency:analyze`

This will find both unused dependencies, and used-and-undeclared ones (when you're using something from a transitive dependency without adding that dependency itself to the `pom.xml`).
Sometimes it reports false positives (for example when a package is loaded dynamically), you can silence them by using `<ignoredUnusedDeclaredDependencies>` in the plugin's config in `pom.xml`.

### Find version updates
I know of two ways to find dependencies that have new versions, and each of them is uniquely flawed.
#### Maven plugin
The first one is using Maven. Run from the project directory:

`mvn versions:display-dependency-updates  "-Dmaven.version.ignore=.*-M.*?,.*-[aA]lpha.*?,.*-[bB]eta.*?"`

Unfortunately, it lists *all* transitive dependencies, including ones from the parent POM (for example, in Manifold it's `spring-boot-starter-parent`, and it has **a lot** of dependencies).
As a result, the plugin's report is very noisy.
#### Manual checking
The second one's flaw is that it takes more time and typing: look up dependencies from `pom.xml` on https://mvnrepository.com/

When choosing a new version, make sure it doesn't have anything serious in the "Vulnerabilities" section (it includes transitive dependencies as well).

#### "Package Checker" plugin
Bonus: a bundled IntelliJ Idea plugin that highlights dependencies with open CVEs (including transitive) when looking at the `pom.xml` file. Doesn't help with non-vulnerable updates, but still very useful.
