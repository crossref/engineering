---
title: "Building Blocks"
linkTitle: "Building Blocks"
weight: 50
description: >
   Static decomposition of the system, abstractions of source-code
tags:
- architecture-diagram
---

{{% pageinfo %}}
**This section is very work-in-progress!**
{{% /pageinfo %}}


This is a very simplified view of our architecture. Other diagrams will contain more detail about individual parts or cross-cutting concerns.

See the [architecture-diagram]({{< ref "/tags/architecture-diagram" >}}) tag for other diagrams.

![Overview](./overview.drawio.svg)