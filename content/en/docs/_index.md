---
title: "Engineering Guide"
linkTitle: "Guide"
weight: 20
menu:
  main:
    weight: 20
---

{{% pageinfo %}}
**This site is a work in progress.**
{{% /pageinfo %}}

[Open Scholarly Infrastructure](https://openscholarlyinfrastructure.org) means building in the open! All of our new source code, internal documentation and bug tracking is open. Everyone is welcome to browse and see how it's put together, but this site is targetted at developers who contribute to the Crossref System.

**If you're a developer trying to use Crossref, or integrate it with your own system, please see the [Crossref documentation](https://www.crossref.org/documentation/) site.**

This site follows the [Arc42](https://arc42.org/) structure. As with all Crossref materials, the content of this site is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).


