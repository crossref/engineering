#!/usr/bin/env bash
set -o errexit   # abort on nonzero exit status
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

MODULE_DIR="./living-docs-modules"
export MODULE_DIR

HUGO_OUTPUT_DIR="content/en/living-documentation"
export HUGO_OUTPUT_DIR

# Convert the Feature: heading to a document heading. The rest of the file is pre-formatted. 
# shellcheck disable=SC2089
RE_FEATURE="s/^Feature: *(.*)$/---\ntitle: \"\1\" \n--- \n<pre>/g"

# shellcheck disable=SC2090
export RE_FEATURE

# Convert gherkin .feature to markdown using some simple text substitutions. 
function convertToMd {
  filePath=$1

  # Only run if this this has a feature heading.
  if grep -q "Feature:" "$filePath"; then
    echo "Convert $filePath to markdown ... "
    
    # The space after -i varies between Mac OS and GNU/Linux versions of sed.
    # No space works on both platforms.
    # To not make a backup file has different syntax between the platforms, so it's easier to just
    # delete after.
    sed -E -i'.tmp' "$RE_FEATURE"  "$filePath"
    echo "</pre>" >> "$filePath"
    rm "$filePath.tmp"
  fi
}

# Enable this to be used later in the find -exec.
export -f convertToMd

# Clone each related repo.
repos=(
  "https://gitlab.com/crossref/keycloak-setup.git"
  "https://gitlab.com/crossref/manifold.git"
  "https://gitlab.com/crossref/mycrossref.git"
)

rm -rf ${MODULE_DIR:?}/*
mkdir -p $MODULE_DIR

for repo in "${repos[@]}"; do 
  (cd $MODULE_DIR && git clone "$repo" --single-branch --depth 1 )
done


# Fetch all hugo 'docsy' template submodule.
git submodule update --init --recursive

# Clear out the content directory except for the index file.
mv "$HUGO_OUTPUT_DIR/_index.md" /tmp/save_index.md
rm -f -r ${HUGO_OUTPUT_DIR:?}/*
mv /tmp/save_index.md "$HUGO_OUTPUT_DIR/_index.md"
mkdir -p $HUGO_OUTPUT_DIR

# Each of these is a git submodule corresponding to a different git project.
echo "module dir: $MODULE_DIR"
for proj in "$MODULE_DIR"/* ; do
  # shellcheck disable=SC2155
  export projName=$(basename "$proj")
  echo "Looking at $projName in $proj"
  export projDir="$HUGO_OUTPUT_DIR/$projName"

  # Get this module's origin URL. Convert git@gitlab.com:crossref/xyz.git to a URL.
  repo=$(cd "$proj" && git config --get remote.origin.url)
  repo="${repo/git@gitlab.com:/https:\/\/gitlab.com\/}"
  repo="${repo/.git//}"

  # Create the directory for this project in the content directory.
  mkdir -p "$projDir"
  echo -e "---\ntitle: $projName\n---\nRepository: [$repo]($repo) \n\n" > "$projDir/_index.md"

  # Copy .feature files and then convert them from Gherkin to Markdown.
  find "$proj" -name "*.feature" -exec bash -c 'export fname="$(basename $0)";cp "$0" "$projDir/${fname/.feature/.md}"' {} \;
  find "$projDir" -name "*.md" -exec bash -c 'convertToMd "$0"' {} \;
done

# Render
hugo

